package radix

import (
	"gitlab.com/mayachain/mayanode/common"
	"gitlab.com/mayachain/mayanode/common/cosmos"
)

const (
	OneOnRadix    = 1000000000000000000
	OneOnMaya     = common.One
	ScalingFactor = OneOnRadix / OneOnMaya
)

// XrdSubunitsToMayaRoundingDown
// Returns the value converted from Radix (18 decimals) to Maya (8 decimals)
// rounded down, if needed. This is used for conversions that affect deposits
// from vault accounts, so that the protocol's books are always less or equal to the actual balance on-chain.
func XrdSubunitsToMayaRoundingDown(value cosmos.Uint) cosmos.Uint {
	return value.QuoUint64(ScalingFactor)
}

// XrdSubunitsToMayaRoundingUp
// Returns the value converted from Radix (18 decimals) to Maya (8 decimals)
// rounded up, if needed. This is used for conversions that affect withdrawals
// from vault accounts (incl. fee payments), so that the protocol's books
// are always less or equal to the actual balance on-chain.
func XrdSubunitsToMayaRoundingUp(value cosmos.Uint) cosmos.Uint {
	roundedDown := value.QuoUint64(ScalingFactor)
	if value.Mod(cosmos.NewUint(ScalingFactor)).IsZero() {
		return roundedDown
	} else {
		return roundedDown.AddUint64(1)
	}
}

func MayaSubunitsToXrd(value cosmos.Uint) cosmos.Uint {
	return value.Mul(cosmos.NewUint(ScalingFactor))
}
