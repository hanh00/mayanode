package mayachain

import (
	"fmt"

	"gitlab.com/mayachain/mayanode/common"
	"gitlab.com/mayachain/mayanode/common/cosmos"
	"gitlab.com/mayachain/mayanode/constants"
	"gitlab.com/mayachain/mayanode/x/mayachain/types"
)

func skimAffiliateFeesV1(ctx cosmos.Context, mgr Manager, mainTx common.Tx, signer cosmos.AccAddress, memoStr string) (cosmos.Uint, error) {
	// Parse memo
	memo, err := ParseMemoWithMAYANames(ctx, mgr.Keeper(), memoStr)
	if err != nil {
		ctx.Logger().Error("fail to parse swap memo", "memo", memoStr, "error", err)
		return cosmos.ZeroUint(), err
	}
	affiliates := memo.GetAffiliates()
	affiliatesBps := memo.GetAffiliatesBasisPoints()
	if len(affiliates) == 0 || len(affiliatesBps) == 0 {
		return cosmos.ZeroUint(), nil
	}

	// initialize swapIndex for affiliate swaps (index 0 is reserved for the main swap)
	swapIndex := 1
	totalDistributed := cosmos.ZeroUint()
	totalSwapAmount := mainTx.Coins[0].Amount
	shares := calculateAffiliateShares(ctx, mgr, totalSwapAmount, affiliates, affiliatesBps)
	for _, share := range shares {
		if !share.amount.IsZero() {
			distributed := distributeShare(ctx, mgr, share, mainTx, signer, &swapIndex)
			totalDistributed = totalDistributed.Add(distributed)
		}
	}
	return totalDistributed, nil
}

func calculateAffiliateSharesV1(ctx cosmos.Context, mgr Manager, inputAmount cosmos.Uint, affiliates []string, affiliatesBps []cosmos.Uint) []affiliateFeeShare {
	// construct a virtual mayaname so we can use the calculateNestedAffiliateShares function for the root affiliates as well
	virtualSubAffiliates := make([]types.MAYANameSubaffiliate, len(affiliates))
	for i := range affiliates {
		virtualSubAffiliates[i].Name = affiliates[i]
		virtualSubAffiliates[i].Bps = affiliatesBps[i]
	}
	virtualMayaname := NewMAYAName("", 0, nil, common.EmptyAsset, nil, cosmos.ZeroUint(), virtualSubAffiliates)
	nestedAffShares, _ := calculateNestedAffiliateShares(ctx, mgr, virtualMayaname, inputAmount)
	return nestedAffShares
}

func calculateNestedAffiliateSharesV1(ctx cosmos.Context, mgr Manager, mayaname MAYAName, inputAmt cosmos.Uint) ([]affiliateFeeShare, cosmos.Uint) {
	keeper := mgr.Keeper()
	// get the direct subaffiliates of the MAYAName
	remainingAmt := inputAmt
	shares := make([]affiliateFeeShare, 0)
	for _, subAff := range mayaname.GetSubaffiliates() {
		subaffIsMayaname := keeper.MAYANameExists(ctx, subAff.Name)
		subaffExplicitAddress := common.NoAddress
		if !subaffIsMayaname {
			var err error
			if subaffExplicitAddress, err = FetchAddress(ctx, keeper, subAff.Name, common.BASEChain); err != nil {
				// remove the subaffiliate from subaffiliate list if invalid or not exists
				ctx.Logger().Info(fmt.Sprintf("invalid subaffiliate %s registered for %s, removing it", subAff.Name, mayaname.Name))
				mayaname.RemoveSubaffiliate(subAff.Name)
				keeper.SetMAYAName(ctx, mayaname)
				continue
			}
		}
		subAmt := common.GetSafeShare(subAff.Bps, cosmos.NewUint(constants.MaxBasisPts), inputAmt)
		ctx.Logger().Debug("affiliate share calculated", "(sub)affiliate", subAff.Name, "bps", subAff.Bps, "amount", subAmt)
		// if total subaffiliate shares exceed 100%, ignore them
		if subAmt.GT(remainingAmt) {
			break
		}
		remainingAmt = remainingAmt.Sub(subAmt)
		if subaffIsMayaname {
			subAffMn, err := keeper.GetMAYAName(ctx, subAff.Name)
			if err != nil {
				ctx.Logger().Error(fmt.Sprintf("fail to get sub-affiliate MAYAName %s for MAYAName %s", subAff.Name, mayaname.Name))
				continue
			}
			nestedAffShares, _ := calculateNestedAffiliateShares(ctx, mgr, subAffMn, subAmt)
			shares = append(shares, nestedAffShares...)
		} else {
			shares = append(shares, affiliateFeeShare{
				mayaname:  types.MAYAName{},
				amount:    subAmt,
				cacaoDest: subaffExplicitAddress,
			})
		}
	}
	if mayaname.Name != "" { // if empty, it is the virtual mayaname
		cacaoDest := common.NoAddress
		if mayaname.PreferredAsset.IsEmpty() {
			cacaoDest = mayaname.GetAlias(common.BASEChain)
			if cacaoDest.IsEmpty() {
				cacaoDest = common.Address(mayaname.Owner.String())
				ctx.Logger().Info("affiliate MAYAName doesn't have native chain alias, owner will be used instead", "mayaname", mayaname.Name, "owner", cacaoDest)
			}
		}
		shares = append(shares, affiliateFeeShare{
			mayaname:  mayaname,
			amount:    remainingAmt,
			cacaoDest: cacaoDest,
		})
		// only for debug log
		address := cacaoDest.String()
		if cacaoDest.IsEmpty() {
			address = fmt.Sprintf("affCol/%s:%s", mayaname.PreferredAsset, mayaname.GetAlias(mayaname.PreferredAsset.Chain))
		}
		ctx.Logger().Debug("affiliate share added", "mayaname", mayaname.Name, "amount", remainingAmt, "address", address)
	}
	return shares, cosmos.ZeroUint()
}

func sendShareV1(ctx cosmos.Context, mgr Manager, share affiliateFeeShare, swapIndex *int) error {
	coin := common.NewCoin(common.BaseNative, share.amount)
	if !share.cacaoDest.IsEmpty() {
		// either no mayaname or no preferred asset
		// send cacao to cacaoDest (mayaname alias/owner or explicit aff address)
		toAccAddress, err := share.cacaoDest.AccAddress()
		if err != nil {
			return fmt.Errorf("fail to convert address into AccAddress, address: %s, error: %w", share.cacaoDest, err)
		}
		// only for debug log
		mayaname := "n/a"
		if share.mayaname.Name != "" {
			mayaname = share.mayaname.Name
		}
		ctx.Logger().Info("sending affiliate fee to affiliate address", "amount", share.amount, "mayaname", mayaname, "address", share.cacaoDest)
		sdkErr := mgr.Keeper().SendFromModuleToAccount(ctx, AsgardName, toAccAddress, common.NewCoins(coin))
		if sdkErr != nil {
			return fmt.Errorf("fail to send native asset to affiliate, address: %s, error: %w", share.cacaoDest, sdkErr)
		}
	} else {
		// preferred asset provided
		// send cacao to affiliate collector
		ctx.Logger().Info("sending affiliate fee to affiliate collector", "amount", share.amount, "mayaname", share.mayaname.Name)
		err := updateAffiliateCollector(ctx, mgr, share, swapIndex)
		if err != nil {
			return fmt.Errorf("failed to send funds to affiliate collector, error: %w", err)
		}
	}
	return nil
}

func swapShareV1(ctx cosmos.Context, mgr Manager, share affiliateFeeShare, mainTx common.Tx, signer cosmos.AccAddress, swapIndex *int) error {
	// Copy mainTx coins so as not to modify the original
	mainTx.Coins = mainTx.Coins.Copy()

	// Construct preferred asset swap tx
	affSwapMsg := NewMsgSwap(
		mainTx,
		common.BaseAsset(),
		share.cacaoDest, // mayaname alias/owner or explicit aff address or NoAddress if preferred address is set
		cosmos.ZeroUint(),
		common.NoAddress,
		cosmos.ZeroUint(),
		"", "", nil,
		MarketOrder,
		0, 0,
		signer,
	)

	affSwapMsg.Tx.Coins[0].Amount = share.amount

	// if cacaoDest is empty that means preferred asset is set
	if share.cacaoDest.IsEmpty() {
		// Set AffiliateCollector Module as destination (toAddress) and populate the AffiliateAddress
		// so that the swap handler can increment the emitted CACAO for the affiliate in the AffiliateCollector
		affiliateColllector, err := mgr.Keeper().GetModuleAddress(AffiliateCollectorName)
		if err != nil {
			return err
		}
		affSwapMsg.AffiliateAddress = common.Address(share.mayaname.Owner.String())
		affSwapMsg.Destination = affiliateColllector // affiliate collector module address

		// trigger preferred asset swap if needed
		checkAndTriggerPreferredAssetSwap(ctx, mgr, share, swapIndex)
	}

	// only for debug log
	mayaname := "n/a"
	if share.mayaname.Name != "" {
		mayaname = share.mayaname.Name
	}
	ctx.Logger().Debug("affiliate fee swap queue", "index", *swapIndex, "amount", share.amount, "mayaname", mayaname, "address", affSwapMsg.Destination)
	// swap the affiliate fee
	if err := mgr.Keeper().SetSwapQueueItem(ctx, *affSwapMsg, *swapIndex); err != nil {
		return fmt.Errorf("fail to add swap to queue, error: %w", err)
	}
	*swapIndex++

	return nil
}
