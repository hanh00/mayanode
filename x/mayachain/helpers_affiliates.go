package mayachain

import (
	"fmt"

	"github.com/blang/semver"
	"gitlab.com/mayachain/mayanode/common"
	"gitlab.com/mayachain/mayanode/common/cosmos"
	"gitlab.com/mayachain/mayanode/constants"
	"gitlab.com/mayachain/mayanode/x/mayachain/types"
)

// affiliate fee share
type affiliateFeeShare struct {
	amount    cosmos.Uint    // amount sent to the affiliate
	cacaoDest common.Address // mayaname alias/owner or explicit native affiliate address or NoAddress if preferred address is set
	mayaname  types.MAYAName //
	// additional fields for events
	parent    string      // parent mayaname or empty string if it's one of the main affiliates
	subFeeBps cosmos.Uint // fee bps of the parent affiliate (or of the swap amount if it's one of the main affiliates)
}

// skimAffiliateFees - attempts to distribute the affiliate fee to the main affiliate in the memo and to each nested subaffiliate.
// Returns the total fee distributed priced in inboundCoin.Asset.
// Logic:
// 1. Parse the memo to get the affiliate mayaname or address and the main affiliate fee
// 2. For affiliate and each subaffiliate
// - If inbound coin is CACAO transfer to the affiliate
// - If inbound coin is not CACAO, swap the coin to CACAO and transfer to the affiliate
// - If affiliate is a mayaname and has a preferred asset, send CACAO to the affiliate collector
func skimAffiliateFees(ctx cosmos.Context, mgr Manager, mainTx common.Tx, signer cosmos.AccAddress, memoStr string) (cosmos.Uint, error) {
	version := mgr.GetVersion()
	switch {
	case version.GTE(semver.MustParse("1.118.0")):
		return skimAffiliateFeesV118(ctx, mgr, mainTx, signer, memoStr)
	default:
		return skimAffiliateFeesV1(ctx, mgr, mainTx, signer, memoStr)
	}
}

func calculateAffiliateShares(ctx cosmos.Context, mgr Manager, inputAmount cosmos.Uint, affiliates []string, affiliatesBps []cosmos.Uint) []affiliateFeeShare {
	version := mgr.GetVersion()
	switch {
	case version.GTE(semver.MustParse("1.118.0")):
		return calculateAffiliateSharesV118(ctx, mgr, inputAmount, affiliates, affiliatesBps)
	default:
		return calculateAffiliateSharesV1(ctx, mgr, inputAmount, affiliates, affiliatesBps)
	}
}

func calculateNestedAffiliateShares(ctx cosmos.Context, mgr Manager, mayaname MAYAName, inputAmt cosmos.Uint) (shares []affiliateFeeShare, remainingAffBps cosmos.Uint) {
	version := mgr.GetVersion()
	switch {
	case version.GTE(semver.MustParse("1.118.0")):
		return calculateNestedAffiliateSharesV118(ctx, mgr, mayaname, inputAmt)
	default:
		return calculateNestedAffiliateSharesV1(ctx, mgr, mayaname, inputAmt)
	}
}

func sendShare(ctx cosmos.Context, mgr Manager, share affiliateFeeShare, swapIndex *int) error {
	version := mgr.GetVersion()
	switch {
	case version.GTE(semver.MustParse("1.118.0")):
		return sendShareV118(ctx, mgr, share, swapIndex)
	default:
		return sendShareV1(ctx, mgr, share, swapIndex)
	}
}

func swapShare(ctx cosmos.Context, mgr Manager, share affiliateFeeShare, mainTx common.Tx, signer cosmos.AccAddress, swapIndex *int) error {
	version := mgr.GetVersion()
	switch {
	case version.GTE(semver.MustParse("1.118.0")):
		return swapShareV118(ctx, mgr, share, mainTx, signer, swapIndex)
	default:
		return swapShareV1(ctx, mgr, share, mainTx, signer, swapIndex)
	}
}

func skimAffiliateFeesV118(ctx cosmos.Context, mgr Manager, mainTx common.Tx, signer cosmos.AccAddress, memoStr string) (cosmos.Uint, error) {
	affiliateFeeTickGranularity := uint64(fetchConfigInt64(ctx, mgr, constants.AffiliateFeeTickGranularity))
	memo, err := ParseMemoWithMAYANames(ctx, mgr.Keeper(), memoStr)
	if err != nil {
		ctx.Logger().Error("fail to parse swap memo", "memo", memoStr, "error", err)
		return cosmos.ZeroUint(), err
	}
	affiliates := memo.GetAffiliates()
	affiliatesBps := memo.GetAffiliatesBasisPoints()
	if len(affiliates) == 0 || len(affiliatesBps) == 0 {
		return cosmos.ZeroUint(), nil
	}

	// initialize swapIndex for affiliate swaps (index 0 is reserved for the main swap)
	swapIndex := 1
	totalDistributed := cosmos.ZeroUint()
	totalSwapAmount := mainTx.Coins[0].Amount
	shares := calculateAffiliateShares(ctx, mgr, totalSwapAmount, affiliates, affiliatesBps)
	for _, share := range shares {
		if !share.amount.IsZero() {
			// fill fields for events
			distributed := distributeShare(ctx, mgr, share, mainTx, signer, &swapIndex)
			totalDistributed = totalDistributed.Add(distributed)

			// Emit affiliate fee events
			// calculate the fee microbps (1/10,000th of a basis point, or 0.000001%)
			feeBpsTick := common.GetSafeShare(
				share.amount,
				totalSwapAmount,
				cosmos.NewUint(constants.MaxBasisPts).MulUint64(affiliateFeeTickGranularity),
			).BigInt().Uint64()

			event := NewEventAffiliateFee(
				mainTx.ID,                         // txId
				mainTx.Memo,                       // memo
				share.mayaname.Name,               // mayaname
				share.cacaoDest,                   // cacaoAddress
				mainTx.Coins[0].Asset,             // asset
				feeBpsTick,                        // feeBpsTick
				totalSwapAmount,                   // grossAmount
				share.amount,                      // feeAmt
				share.parent,                      // parent
				share.subFeeBps.BigInt().Uint64(), // subFeeBps
			)
			if err = mgr.EventMgr().EmitEvent(ctx, event); err != nil {
				ctx.Logger().Error("fail to emit affiliate fee event", "error", err)
			}
			// if mayaname preferred asset is empty or cacao, release the affiliate collector
			if share.mayaname.Name != "" && (share.mayaname.PreferredAsset.IsEmpty() || share.mayaname.PreferredAsset.IsNativeBase()) {
				ctx.Logger().Info(fmt.Sprintf("Releasing affiliate collector on swap for %s", share.mayaname.Name))
				if err = releaseAffiliateCollector(ctx, mgr, share.mayaname); err != nil {
					ctx.Logger().Error(fmt.Sprintf("failed to release affiliate collector funds for %s. Error: %s", share.mayaname.Name, err))
				}
			}
		}
	}

	return totalDistributed, nil
}

func calculateAffiliateSharesV118(ctx cosmos.Context, mgr Manager, inputAmount cosmos.Uint, affiliates []string, affiliatesBps []cosmos.Uint) []affiliateFeeShare {
	// construct a virtual mayaname so we can use the calculateNestedAffiliateShares function for the root affiliates as well
	virtualSubAffiliates := make([]types.MAYANameSubaffiliate, len(affiliates))
	for i := range affiliates {
		virtualSubAffiliates[i].Name = affiliates[i]
		virtualSubAffiliates[i].Bps = affiliatesBps[i]
	}
	virtualMayaname := NewMAYAName("", 0, nil, common.EmptyAsset, nil, cosmos.ZeroUint(), virtualSubAffiliates)
	shares, _ := calculateNestedAffiliateShares(ctx, mgr, virtualMayaname, inputAmount)
	return shares
}

func calculateNestedAffiliateSharesV118(ctx cosmos.Context, mgr Manager, mayaname MAYAName, inputAmt cosmos.Uint) (shares []affiliateFeeShare, remainingAffBps cosmos.Uint) {
	affiliateFeeTickGranularity := uint64(fetchConfigInt64(ctx, mgr, constants.AffiliateFeeTickGranularity))
	var err error
	keeper := mgr.Keeper()
	maxBps := cosmos.NewUint(constants.MaxBasisPts)
	if mayaname.Name == "" {
		// for the root affiliates the total max allowed bps is MaxAffiliateFeeBasisPoints (5%)
		remainingAffBps = cosmos.NewUint(uint64(fetchConfigInt64(ctx, mgr, constants.MaxAffiliateFeeBasisPoints)))
	} else {
		// for sub-affiliates the total max allowed bps is MaxBasisPts (100%)
		remainingAffBps = maxBps
	}
	// get the direct subaffiliates of the MAYAName
	shares = make([]affiliateFeeShare, 0)
	for _, subAff := range mayaname.GetSubaffiliates() {
		var subAffMayaname MAYAName
		cacaoDest := common.NoAddress
		// if subaffiliate is a mayaname, fetch it
		if keeper.MAYANameExists(ctx, subAff.Name) {
			if subAffMayaname, err = keeper.GetMAYAName(ctx, subAff.Name); err != nil {
				ctx.Logger().Error(fmt.Sprintf("fail to get mayaname %s", subAff.Name))
				continue
			}
			// if preferred asset is set, leave the cacaDest empty (fee will be sent to affiliate collector)
			// else set it to cacao alias or owner
			if subAffMayaname.PreferredAsset.IsEmpty() {
				// fetch the cacao address of the mayaname
				cacaoDest = subAffMayaname.GetAlias(common.BASEChain)
				if cacaoDest.IsEmpty() {
					cacaoDest = common.Address(subAffMayaname.Owner.String())
					ctx.Logger().Info("affiliate MAYAName doesn't have native chain alias, owner will be used instead", "mayaname", subAffMayaname.Name, "owner", cacaoDest)
				}
			}
		} else {
			// if subaffiliate is not a mayaname, fetch the cacao address
			if cacaoDest, err = FetchAddress(ctx, keeper, subAff.Name, common.BASEChain); err != nil {
				// remove the subaffiliate from subaffiliate list if invalid or not exists
				ctx.Logger().Info(fmt.Sprintf("invalid subaffiliate %s registered for %s, removing it", subAff.Name, mayaname.Name))
				mayaname.RemoveSubaffiliate(subAff.Name)
				keeper.SetMAYAName(ctx, mayaname)
				continue
			}
		}
		// if the remaining bps (starting from 100%) is less than the current subaff bps means that
		/// the sum of subaffiliate shares exceed 100%, in this case ignore this and all subsequent subaffiliate
		if remainingAffBps.LT(subAff.Bps) {
			ctx.Logger().Error(fmt.Sprintf("sum of subaffiliate shares bps exceeds %s on subaffiliate %s", maxBps, subAff.Name))
			break
		}
		remainingAffBps = remainingAffBps.Sub(subAff.Bps)
		finalAffBps := subAff.Bps.MulUint64(affiliateFeeTickGranularity) // increase sub-affiliate bps calculation granularity for more precise distribution
		// process the subaffiliates if there are any
		if subAffMayaname.Name != "" && len(subAffMayaname.Subaffiliates) > 0 {
			// calculate the amount for this affiliate together with it's sub-affiliates
			subAmt := common.GetSafeShare(subAff.Bps, maxBps, inputAmt)
			// add subaffiliates' shares first
			subShares, remainingSubAffBps := calculateNestedAffiliateShares(ctx, mgr, subAffMayaname, subAmt)
			shares = append(shares, subShares...)
			// now add this affiliate as well with the remaining bps from subaffs (eg dog has subaff cat with subaffbps 40%, dog gets 60%)
			finalAffBps = remainingSubAffBps.Mul(subAff.Bps).QuoUint64(affiliateFeeTickGranularity)
		}
		subAmt := common.GetSafeShare(finalAffBps, maxBps.MulUint64(affiliateFeeTickGranularity), inputAmt)
		ctx.Logger().Debug("affiliate share calculated", "(sub)affiliate", subAff.Name, "bps", finalAffBps, "amount", subAmt)
		shares = append(shares, affiliateFeeShare{
			mayaname:  subAffMayaname,
			amount:    subAmt,
			cacaoDest: cacaoDest,
			// event fields
			parent:    mayaname.Name,
			subFeeBps: finalAffBps,
		})
	}

	return shares, remainingAffBps
}

func distributeShare(ctx cosmos.Context, mgr Manager, share affiliateFeeShare, mainTx common.Tx, signer cosmos.AccAddress, swapIndex *int) cosmos.Uint {
	var err error
	if mainTx.Coins[0].Asset.IsNativeBase() {
		err = sendShare(ctx, mgr, share, swapIndex)
	} else {
		err = swapShare(ctx, mgr, share, mainTx, signer, swapIndex)
	}
	if err != nil {
		// just log the error, fee distribution to other affiliates will continue
		var to string
		if share.mayaname.Name != "" {
			to = share.mayaname.Name
		} else {
			to = share.cacaoDest.String()
		}
		ctx.Logger().Error("fail to distribute affiliate fee share", "to", to, "error", err)
		return cosmos.ZeroUint()
	}

	return share.amount
}

func sendShareV118(ctx cosmos.Context, mgr Manager, share affiliateFeeShare, swapIndex *int) error {
	coin := common.NewCoin(common.BaseNative, share.amount)
	if !share.cacaoDest.IsEmpty() {
		// either no mayaname or no preferred asset
		// send cacao to cacaoDest (mayaname alias/owner or explicit aff address)
		toAccAddress, err := share.cacaoDest.AccAddress()
		if err != nil {
			return fmt.Errorf("fail to convert address into AccAddress, address: %s, error: %w", share.cacaoDest, err)
		}
		ctx.Logger().Info("sending affiliate fee to affiliate address", "amount", share.amount, "mayaname", share.mayaname.Name, "address", share.cacaoDest)
		sdkErr := mgr.Keeper().SendFromModuleToAccount(ctx, AsgardName, toAccAddress, common.NewCoins(coin))
		if sdkErr != nil {
			return fmt.Errorf("fail to send native asset from %s module to affiliate, address: %s, error: %w", AsgardName, share.cacaoDest, sdkErr)
		}
	} else {
		// preferred asset provided
		// send cacao to affiliate collector
		ctx.Logger().Info("sending affiliate fee to affiliate collector", "amount", share.amount, "mayaname", share.mayaname.Name)
		err := updateAffiliateCollector(ctx, mgr, share, swapIndex)
		if err != nil {
			return fmt.Errorf("failed to send funds to affiliate collector, error: %w", err)
		}
	}
	return nil
}

func swapShareV118(ctx cosmos.Context, mgr Manager, share affiliateFeeShare, mainTx common.Tx, signer cosmos.AccAddress, swapIndex *int) error {
	// Copy mainTx coins so as not to modify the original
	mainTx.Coins = mainTx.Coins.Copy()

	// Construct preferred asset swap tx
	affSwapMsg := NewMsgSwap(
		mainTx,
		common.BaseAsset(),
		share.cacaoDest, // mayaname alias/owner or explicit aff address or NoAddress if preferred address is set
		cosmos.ZeroUint(),
		common.NoAddress,
		cosmos.ZeroUint(),
		"", "", nil,
		MarketOrder,
		0, 0,
		signer,
	)

	affSwapMsg.Tx.Coins[0].Amount = share.amount

	// if cacaoDest is empty that means preferred asset is set
	if share.cacaoDest.IsEmpty() {
		// Set AffiliateCollector Module as destination (toAddress) and populate the AffiliateAddress
		// so that the swap handler can increment the emitted CACAO for the affiliate in the AffiliateCollector
		affiliateColllector, err := mgr.Keeper().GetModuleAddress(AffiliateCollectorName)
		if err != nil {
			return err
		}
		affSwapMsg.AffiliateAddress = common.Address(share.mayaname.Owner.String())
		affSwapMsg.Destination = affiliateColllector // affiliate collector module address

		// trigger preferred asset swap if needed
		checkAndTriggerPreferredAssetSwap(ctx, mgr, share, swapIndex)
	}
	ctx.Logger().Debug("affiliate fee swap queue", "index", *swapIndex, "amount", share.amount, "mayaname", share.mayaname.Name, "address", affSwapMsg.Destination)
	// swap the affiliate fee
	if err := mgr.Keeper().SetSwapQueueItem(ctx, *affSwapMsg, *swapIndex); err != nil {
		return fmt.Errorf("fail to add swap to queue, error: %w", err)
	}
	*swapIndex++

	return nil
}

func updateAffiliateCollector(ctx cosmos.Context, mgr Manager, share affiliateFeeShare, swapIndex *int) error {
	affCol, err := mgr.Keeper().GetAffiliateCollector(ctx, share.mayaname.Owner)
	if err != nil {
		return fmt.Errorf("failed to get affiliate collector, MAYAName %s, error: %w", share.mayaname.Name, err)
	}
	coin := common.NewCoin(common.BaseNative, share.amount)
	if err := mgr.Keeper().SendFromModuleToModule(ctx, AsgardName, AffiliateCollectorName, common.NewCoins(coin)); err != nil {
		return fmt.Errorf("failed to send funds to affiliate collector, error: %w", err)
	} else {
		affCol.CacaoAmount = affCol.CacaoAmount.Add(coin.Amount)
		mgr.Keeper().SetAffiliateCollector(ctx, affCol)
	}
	checkAndTriggerPreferredAssetSwap(ctx, mgr, share, swapIndex)
	return nil
}

func checkAndTriggerPreferredAssetSwap(ctx cosmos.Context, mgr Manager, share affiliateFeeShare, swapIndex *int) {
	affCol, err := mgr.Keeper().GetAffiliateCollector(ctx, share.mayaname.Owner)
	if err != nil {
		ctx.Logger().Error("failed to get affiliate collector", "MAYAName", share.mayaname.Name, "error", err)
		return
	}
	// Trigger a preferred asset swap if the accrued CACAO exceeds the threshold:
	// Threshold = PreferredAssetOutboundFeeMultiplier (default = 100) x current outbound fee of the preferred asset chain.
	// If the affiliate collector's CACAO amount exceeds the threshold, initiate the preferred asset swap.
	threshold := getPreferredAssetSwapThreshold(ctx, mgr, share.mayaname.PreferredAsset)
	if affCol.CacaoAmount.GT(threshold) {
		ctx.Logger().Info("preferred asset swap triggered", "mayaname", share.mayaname.Name, "prefAsset", share.mayaname.PreferredAsset, "threshold", threshold, "affcol amount", affCol.CacaoAmount)
		if err = triggerPreferredAssetSwap(ctx, mgr, share.mayaname, affCol, *swapIndex); err != nil {
			ctx.Logger().Error("fail to swap to preferred asset", "mayaname", share.mayaname.Name, "err", err)
		} else {
			*swapIndex++
		}
	} else {
		ctx.Logger().Info("preferred asset swap not yet triggered", "mayaname", share.mayaname.Name, "prefAsset", share.mayaname.PreferredAsset, "threshold", threshold, "affcol amount", affCol.CacaoAmount)
	}
}

// not part of AffiliateFeeDistributor
func releaseAffiliateCollector(ctx cosmos.Context, mgr Manager, mayaname MAYAName) error {
	var err error
	var destAcc cosmos.AccAddress
	affCol, err := mgr.Keeper().GetAffiliateCollector(ctx, mayaname.Owner)
	if err != nil {
		return fmt.Errorf("fail to get affiliate collector: %w", err)
	}
	if affCol.CacaoAmount.IsZero() {
		return nil
	}
	coins := common.NewCoins(common.NewCoin(common.BaseNative, affCol.CacaoAmount))
	destAddr := mayaname.GetAlias(common.BASEChain)
	if destAddr.IsEmpty() {
		destAcc = mayaname.Owner
	} else {
		destAcc, err = destAddr.AccAddress()
		if err != nil {
			return fmt.Errorf("fail to convert address into AccAddress, address: %s, error: %w", destAddr, err)
		}
	}
	sdkErr := mgr.Keeper().SendFromModuleToAccount(ctx, AffiliateCollectorName, destAcc, coins)
	if sdkErr != nil {
		return fmt.Errorf("fail to send native asset from %s module to affiliate, address: %s, error: %w", AffiliateCollectorName, destAcc, sdkErr)
	}

	ctx.Logger().Info(fmt.Sprintf("Affiliate collector amount (%s CACAO) successfully released for %s", affCol.CacaoAmount, mayaname.Name))

	affCol.CacaoAmount = cosmos.ZeroUint()
	mgr.Keeper().SetAffiliateCollector(ctx, affCol)

	return nil
}
